/*
 * CryptoLayer.h
 *
 *  Created on: Feb 14, 2022
 *      Author: josel
 */

#ifndef CRYPTOLAYER_H_
#define CRYPTOLAYER_H_

#include "lwip/opt.h"



#include "lwip/sys.h"
#include "lwip/api.h"

#include "aes.h"
#include "fsl_crc.h"


typedef enum
{
	NO_ERROR_E = 0,
	INVALID_CRC_TYPE_E,
	CRC_FAILED_E,
	TX_FAILED_E,
	CONNECTION_FAILED_E
}CryptoError_e;




typedef struct
{
	uint8_t CrcUsage;
	uint8_t AesUsage;
}ConfigBits_st;


CryptoError_e CryptoInit(struct netconn **conn);

CryptoError_e CryptoReceive(uint8_t *DataBuff,struct netconn *conn,uint16_t *Length);

CryptoError_e CryptoSend(uint8_t *DataBuff,struct netconn *conn, uint16_t Length);



#endif /* CRYPTOLAYER_H_ */
