/*
 * CryptoLayer.c
 *
 *  Created on: Feb 14, 2022
 *      Author: josel
 */
#include <stdio.h>




#include "CryptoLayer.h"
#include "board.h"





struct AES_ctx AES_struct;

extern const uint8_t Aes_Key[];
extern const uint8_t Aes_IV[];


static void InitCrc32(CRC_Type *base, uint32_t seed)
{
    crc_config_t config;

    config.polynomial         = 0x04C11DB7U;
    config.seed               = seed;
    config.reflectIn          = true;
    config.reflectOut         = true;
    config.complementChecksum = true;
    config.crcBits            = kCrcBits32;
    config.crcResult          = kCrcFinalChecksum;

    CRC_Init(base, &config);
}


CryptoError_e CryptoInit(struct netconn **conn)
{
	struct netconn *TempConn;
	CryptoError_e ReturnValue = NO_ERROR_E;
	err_t err;



	AES_init_ctx_iv(&AES_struct, Aes_Key, Aes_IV);

	TempConn = netconn_new(NETCONN_TCP);
	netconn_bind(TempConn, IP_ADDR_ANY, 10000);
	netconn_listen(TempConn);

	err = netconn_accept(TempConn, conn);
	if(ERR_OK != err)
	{
		ReturnValue = CONNECTION_FAILED_E;
	}

	return ReturnValue;

}

CryptoError_e CryptoReceive(uint8_t *DataBuff,struct netconn *conn,uint16_t *Length)
{
	uint32_t Crc_32;
	CryptoError_e ReturnValue = NO_ERROR_E;
	struct netbuf *buf;
	err_t err;
	static uint8_t *ReceivedDataPtr;

	uint16_t Index = 0;

	AES_init_ctx_iv(&AES_struct, Aes_Key, Aes_IV);
	if((err = netconn_recv(conn, &buf)) == ERR_OK)
	{
		netbuf_data(buf,(void**)&ReceivedDataPtr,Length);
		Crc_32 = (uint32_t)ReceivedDataPtr[(*Length)-4]|(uint32_t)ReceivedDataPtr[(*Length)-3]<<8|(uint32_t)ReceivedDataPtr[(*Length)-2]<<16|(uint32_t)ReceivedDataPtr[(*Length)-1]<<24;
		InitCrc32(CRC0,0xFFFFFFFFU);
		CRC_WriteData(CRC0, (uint8_t *)&ReceivedDataPtr[0], (*Length)-4);
		if(CRC_Get32bitResult(CRC0) != Crc_32)
		{
			ReturnValue = CRC_FAILED_E;
		}
		else
		{
			(*Length) -=4;
			memcpy(DataBuff,ReceivedDataPtr, *Length);
			AES_CBC_decrypt_buffer(&AES_struct,DataBuff,*Length);
			for(Index=0;Index<*Length;Index++)
			{
				PRINTF("%c",DataBuff[Index]);
			}
			PRINTF("\r\n");
		}
	}
	netbuf_delete(buf);
	return ReturnValue;

}

CryptoError_e CryptoSend(uint8_t *DataBuff,struct netconn *conn, uint16_t Length)
{
	uint32_t Crc_32;
	CryptoError_e ReturnValue = NO_ERROR_E;
	struct netbuf *buf;
	err_t err;
	uint8_t PaddedMessage[256]={0};


	AES_init_ctx_iv(&AES_struct, Aes_Key, Aes_IV);

	if(Length%16)
	{
		Length = Length + (16-(Length%16));
	}
	memcpy(PaddedMessage,DataBuff,Length);
	AES_CBC_encrypt_buffer(&AES_struct, PaddedMessage, Length);
	InitCrc32(CRC0,0xFFFFFFFFU);
	CRC_WriteData(CRC0, (uint8_t *)&PaddedMessage[0], Length);
	Crc_32 = CRC_Get32bitResult(CRC0);
	PaddedMessage[Length] = (uint32_t)(Crc_32 & 0xFF);
	PaddedMessage[Length+1] = (uint32_t)((Crc_32>>8U) & 0xFF);
	PaddedMessage[Length+2] = (uint32_t)((Crc_32>>16U) & 0xFF);
	PaddedMessage[Length+3] = (uint32_t)((Crc_32>>24U) & 0xFF);

	if(err = netconn_write(conn, PaddedMessage, Length+4, NETCONN_COPY)!= ERR_OK)
	{
		ReturnValue = TX_FAILED_E;
	}

	return ReturnValue;
}

