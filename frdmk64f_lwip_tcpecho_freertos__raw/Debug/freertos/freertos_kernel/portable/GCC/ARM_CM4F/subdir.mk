################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../freertos/freertos_kernel/portable/GCC/ARM_CM4F/port.c 

OBJS += \
./freertos/freertos_kernel/portable/GCC/ARM_CM4F/port.o 

C_DEPS += \
./freertos/freertos_kernel/portable/GCC/ARM_CM4F/port.d 


# Each subdirectory must supply rules for building sources it contributes
freertos/freertos_kernel/portable/GCC/ARM_CM4F/%.o: ../freertos/freertos_kernel/portable/GCC/ARM_CM4F/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -std=gnu99 -D__REDLIB__ -DCPU_MK64FN1M0VLL12 -DCPU_MK64FN1M0VLL12_cm4 -DUSE_RTOS=1 -DPRINTF_ADVANCED_ENABLE=1 -DFRDM_K64F -DFREEDOM -DLWIP_DISABLE_PBUF_POOL_SIZE_SANITY_CHECKS=1 -DSERIAL_PORT_TYPE_UART=1 -DSDK_OS_FREE_RTOS -DMCUXPRESSO_SDK -DSDK_DEBUGCONSOLE=1 -DCR_INTEGER_PRINTF -DPRINTF_FLOAT_ENABLE=0 -D__MCUXPRESSO -D__USE_CMSIS -DDEBUG -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\frdmk64f_lwip_tcpecho_freertos__raw\board" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\frdmk64f_lwip_tcpecho_freertos__raw\source" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\frdmk64f_lwip_tcpecho_freertos__raw\mdio" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\frdmk64f_lwip_tcpecho_freertos__raw\phy" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\frdmk64f_lwip_tcpecho_freertos__raw\lwip\contrib\apps\tcpecho" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\frdmk64f_lwip_tcpecho_freertos__raw\lwip\port" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\frdmk64f_lwip_tcpecho_freertos__raw\lwip\src" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\frdmk64f_lwip_tcpecho_freertos__raw\lwip\src\include" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\frdmk64f_lwip_tcpecho_freertos__raw\drivers" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\frdmk64f_lwip_tcpecho_freertos__raw\utilities" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\frdmk64f_lwip_tcpecho_freertos__raw\device" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\frdmk64f_lwip_tcpecho_freertos__raw\component\uart" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\frdmk64f_lwip_tcpecho_freertos__raw\component\serial_manager" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\frdmk64f_lwip_tcpecho_freertos__raw\component\lists" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\frdmk64f_lwip_tcpecho_freertos__raw\CMSIS" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\frdmk64f_lwip_tcpecho_freertos__raw\freertos\freertos_kernel\include" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\frdmk64f_lwip_tcpecho_freertos__raw\freertos\freertos_kernel\portable\GCC\ARM_CM4F" -O0 -fno-common -g3 -c -ffunction-sections -fdata-sections -ffreestanding -fno-builtin -fmerge-constants -fmacro-prefix-map="../$(@D)/"=. -mcpu=cortex-m4 -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


